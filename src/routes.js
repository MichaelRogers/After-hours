import React from 'react';
import {Router, Route, hashHistory, IndexRoute} from 'react-router';
import Home from './containers/Home/Home';
import App from './containers/App/App';
import About from './containers/About/About';
import Contact from './containers/Contact/Contact';

export default (
    <Router history = {hashHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={Home}/>
            <Route path="/about" component={About}/>
            <Route path="/contact" component={Contact}/>
        </Route>
    </Router>
);