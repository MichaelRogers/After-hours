import React, {Component, PropTypes} from 'react';
import {} from './Home.style';
import App from '../App/App';

export default class Home extends Component{
    render(){
        return(
            <App>
                <h1>Homepage</h1>
            </App>
        );
    }
}